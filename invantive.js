$invantive = function() {}

$invantive.configureAngular = function(app)
{
         app.config(['$locationProvider', function($locationProvider) { 
            $locationProvider.html5Mode({ enabled: true, requireBase: false }); 
        }])
        
        //
        // When an error is thrown, show error modal.
        //
        app.factory('errorInterceptor', function ($q) {
        var preventFurtherRequests = false;

        return {
            request: function (config) {
                if (preventFurtherRequests) {
                    return;
                }
                return config || $q.when(config);
            },
            requestError: function(request){
                return $q.reject(request);
            },
            response: function (response) {
                return response || $q.when(response);
            },
            responseError: function (response) {
                if (response && response.status === 500) {
                    preventFurtherRequests = true;
                    $('.errorMessage.er500, .errorOverlay').fadeIn();
                    var errorData = response.data;
                    var errorDataStripped = errorData.substring(errorData.lastIndexOf("<body>")+6,errorData.lastIndexOf("</body>"));
                    $( errorDataStripped ).appendTo($('.errorMessage')); 
                }
                return $q.reject(response);
            }
        };
        });
        
        app.config(function ($httpProvider) {
            $httpProvider.interceptors.push('errorInterceptor');    
        });

};

