use select code from systemdivisions where vatnumber = :vatnumber
;
select /*+ join_set(act, id, 5000) */ 
       tle.division
,      tle.financialyear
,      tle.financialperiod
,      tle.amountdc
,      tle.amountvatfc
,      tle.description
,      tle.date
,      tle.entrynumber
,      tle.linenumber
,      tle.glaccountcode
,      tle.glaccountdescription
,      tle.invoicenumber
,      tle.itemcode
,      tle.journalcode
,      tle.type
,      tle.VATCode
,      tle.vatcodedescription
,      tle.VatPercentage
,      tle.paymentreference
,      tle.yourref
,      tle.accountcode
,      tle.accountname
,      act.addressline1
,      act.addressline2
,      act.city
,      act.country
,      act.email
,      act.postcode
,      act.vatnumber
,      act.chamberofcommerce
,      act.phone
,      act.website
,      act.purchasevatcode
,      act.purchasevatcodedescription
,      act.salesvatcode
,      act.salesvatcodedescription
from   transactionlinesbulk tle
left 
outer
join   exactonlinerest..accounts act
on     act.id       = tle.account
and    act.division = tle.division
where  tle.financialyear   = :finyear
and    tle.financialperiod between :periodstart and :periodend
and    tle.type in (20 /* Invoice */,21 /* Credit note */, 30 /* Purchase */, 31 /* Purchase Credit */)
and    tle.linenumber not in (0, 9999)
and    not (tle.amountdc = 0 and tle.amountvatfc = 0 )
order
by     tle.division
,      tle.entrynumber
,      tle.linenumber