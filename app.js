//
// Set app and controller.
//
var app = angular.module('vatBox', ['cgBusy', 'pascalprecht.translate', 'ngSanitize']);
app.controller('vat', function ($scope, $http, $translate, $location, $window)
{
    var countryCode = $location.search().country;

    var countryNl = {"Code": "nl", "Label": "Nederland"};
    var countryBe = {"Code": "be", "Label": "België / Belgique"};
    var countryFr = {"Code": "fr", "Label": "France"};
    var countryGb = {"Code": "gb", "Label": "Great Britain"};
    var countryDe = {"Code": "de", "Label": "Deutschland"};
    var countryEs = {"Code": "es", "Label": "España"};
    var countryUs = {"Code": "us", "Label": "United States"};

    $scope.myCountries = [countryNl, countryBe, countryFr, countryGb, countryDe, countryEs, countryUs];
    
    $scope.selectedCountry = countryNl.Code; 
    
    $scope.updateSelectedCountry = function()
    {
        //
        // Get user information for picture and name in header.
        //
        var url = "auth?preset=" + countryCode + "-btw-me-query" + "&returnUrl=" + encodeURIComponent(window.location.href);

        $scope.spinnerGet = $http.get(url)
               .then
                  ( function successCallback(response)
                        {
                          var isAuthenticated = response.data.isAuthenticated;
                          var authenticationUrl = response.data.authenticationUrl;

                          if (isAuthenticated)
                          {
                            //
                            // Get user information for picture and name in header.
                            //
                            $scope.spinnerGet = $http.get("Preset?preset=" + countryCode + "-btw-me-query")
                                   .then
                                      ( function successCallback(response)
                                            {
                                              var me = response.data.Results[0].Data[0];
                                            
                                              var image = me.ThumbnailPicture;
                                              var imageFormat = me.ThumbnailPictureFormat;
                                              $('#username').html(me.FullName);
                                              $('#usercompany').html(me.DivisionCustomerName);
                                              $('#userimage').attr('src', 'data:image/' + imageFormat + ';base64,' + image);
                                              _paq.push(['setUserId', 'Name: ' + me.FullName + ' E-mail: ' + me.Email + ' Division Customer: ' + me.DivisionCustomerName]);
                                              $translate.use(me.LanguageCode.substring(0, 2));
                                            }
                                      , function errorCallback(response)
                                            {
                                                alert('Could not load user information.');
                                            }
                                      );
                          }
                          else
                          {
                              window.location.href = authenticationUrl;
                          }
                        }
                  , function errorCallback(response)
                        {
                            alert('Could not load authentication information.');
                        }
                  );
    }
        
    if (countryCode == undefined)
    {
        $('.slideIn, .slideIn2, .slideIn3').fadeOut(0);
        $('.slideIn4').fadeIn();
    }
    else
    {
        $scope.updateSelectedCountry();
    }

    $scope.startSingle = function()
    {
        //
        // Set division URL for Angular scope.
        //          
        var urlDivision = "Preset?preset=" + countryCode + "-btw-division-query";
        $scope.spinnerGet = $http.get(urlDivision).then(function (response)
        {
            //
            // Get systemdivisions with DAP query from set URL.
            //
            $scope.myDivisions = response.data.Results[0].Data;
            //
            // Set the first result as default selected option for Division.
            //
            $scope.division = "" + $scope.myDivisions[0].Code;
            //
            // Initiate showYear function to make it pop up without changing an option in the Division dropdown.
            //
            $scope.showYear();
        });
        
        $scope.showYear = function()
        {
            //
            // Set finyear URL for Angular scope.
            //
            var urlYear = "Preset?preset=" + countryCode + "-btw-year-query&division=" + $scope.division;
            
            $scope.spinnerGet = $http.get(urlYear).then(function (response)
            {
                //
                // Get finyears with DAP query from set URL.
                //
                $scope.myYears = response.data.Results[0].Data;
                //
                // Set the first result as default selected option.
                //
                $scope.year = "" + $scope.myYears[0].FINYEAR;
                //
                // Initiate showPeriod function to make it pop up withouth changing an option in the Year dropdown.
                //
                $scope.showPeriod();
            });
            
        }      
        
        $scope.showPeriod = function()
        {
            //
            // Set finperiod URL's for Angular scope.
            //
            var urlPeriod = "Preset?preset=" + countryCode + "-btw-period-query&division=" + $scope.division + "&finyear=" + $scope.year;
            
            $scope.spinnerGet = $http.get(urlPeriod).then(function (response)
            {
                //
                // Get financial periods.
                //
                $scope.myPeriods = response.data.Results[0].Data;
                //
                // Set the first result as default selected option for Period Start.
                //
                $scope.periodstart = "" + $scope.myPeriods[0].FinPeriod;
                //
                // Set the last result as default selected option for Period End.
                //
                $scope.periodend = "" + $scope.myPeriods[$scope.myPeriods.length - 1].FinPeriod;
            });
            
        }

        //
        // Store URL with all variables.
        //
        $scope.downloadCsv = function() {
            var urlDownload = "Preset?preset=" + countryCode + "-btw-download-csv-query&division=" + $scope.division + "&finyear=" + $scope.year + "&periodstart=" + $scope.periodstart + "&periodend=" + $scope.periodend;
            //
            // When requestButton is clicked, download CSV and create filename.
            //
                var divisionLabel = $(".division option:selected").text();
                var fileName = divisionLabel + " " + $scope.year + " " + " van " + $scope.periodstart + " tot en met " + $scope.periodend;
                var anchor = document.createElement("a");
                anchor.download = fileName + ".csv";
                anchor.href = urlDownload;
                anchor.click();
                $scope.spinnerGet = $http.get(urlDownload)
        }
    }
    
    $scope.startVat = function() {
        //
        // Repeat for VAT Entities
        //
        var urlTax = "Preset?preset=" + countryCode + "-btw-taxentity-query";
        $scope.spinnerGet = $http.get(urlTax).then(function (response)
        {
            $scope.myTax = response.data.Results[0].Data;
            $scope.tax = "" + $scope.myTax[0].VATNUMBER;
            $scope.code_first = "" + $scope.myTax[0].code_first;
            $scope.showYearFis();
        });
        $scope.showYearFis = function()
        {
            var urlYearFis = "Preset?preset=" + countryCode + "-btw-year-query&division=" + $scope.code_first;
            $scope.spinnerGet = $http.get(urlYearFis).then(function (response)
            {
                $scope.myYearsFis = response.data.Results[0].Data;
                $scope.yearfis = "" + $scope.myYearsFis[0].FINYEAR;
                $scope.showPeriodFis();
            });
        }
        $scope.showPeriodFis = function()
        {
            var urlPeriodFis = "Preset?preset=" + countryCode + "-btw-period-query&division=" + $scope.code_first + "&finyear=" + $scope.yearfis;
            $scope.spinnerGet = $http.get(urlPeriodFis).then(function (response)
            {
                $scope.myPeriodsFis = response.data.Results[0].Data;
                $scope.periodfisstart = "" + $scope.myPeriodsFis[0].FinPeriod;
                $scope.periodfisend = "" + $scope.myPeriodsFis[$scope.myPeriodsFis.length - 1].FinPeriod;
            });
            
        }
        $scope.downloadCsvFis = function() {
                var urlDownloadFis = "Preset?preset=" + countryCode + "-btw-download-csv-fis-query&vatnumber=" + $scope.tax + "&finyear=" + $scope.yearfis + "&periodstart=" + $scope.periodfisstart + "&periodend=" + $scope.periodfisend;
                var taxLabel = $(".tax option:selected").text();
                var fileName = taxLabel + " " + $scope.yearfis + " " + " van " + $scope.periodfisstart + " tot en met " + $scope.periodfisend;
                var anchor = document.createElement("a");
                anchor.download = fileName + ".csv";
                anchor.href = urlDownloadFis;
                anchor.click();
                $scope.spinnerGet = $http.get(urlDownloadFis);
        }
    }
});
  
app.config(['$translateProvider', function ($translateProvider)
{
    $translateProvider.preferredLanguage('en');
    $translateProvider.addInterpolation('$translateMessageFormatInterpolation');
    $translateProvider.useSanitizeValueStrategy('sanitize');
   
    $translateProvider.translations('en', {
        itgen_bt_heading: 'VAT Toolkit',
        itgen_bt_toolname_div: 'VAT Entry Lines per Company',
        itgen_bt_toolname_fiscal: 'VAT Entry Lines per Fiscal Unit',
        itgen_bt_button_vat: 'VAT Entry Lines',
        itgen_bt_other: 'More products by Invantive',
        itgen_bt_fiscal_year: 'Fiscal Year',
        itgen_bt_period_from : 'Period from:',
        itgen_bt_up_to : 'up to and including:',
        itgen_bt_division: 'Company:',
        itgen_bt_fiscal_entity: 'Fiscal Unit:',
        itgen_bt_choose_division: 'Choose company and period',
        itgen_bt_choose_fiscal_entity: 'Choose fiscal unit and period',
        itgen_bt_powered: 'Powered by Invantive',
        itgen_ct_country: 'Country:',
        itgen_ct_choose_country: 'Choose country',
    });
   
    $translateProvider.translations('de', {
        itgen_bt_heading: 'VAT Toolkit',
        itgen_bt_toolname_div: 'VAT Entry Lines per Company',
        itgen_bt_toolname_fiscal: 'VAT Entry Lines per Fiscal Unit',
        itgen_bt_button_vat: 'VAT Entry Lines',
        itgen_bt_other: 'More products by Invantive',
        itgen_bt_fiscal_year: 'Fiscal Year',
        itgen_bt_period_from : 'Period from:',
        itgen_bt_up_to : 'up to and including:',
        itgen_bt_division: 'Company:',
        itgen_bt_fiscal_entity: 'Fiscal Unit:',
        itgen_bt_choose_division: 'Choose company and period',
        itgen_bt_choose_fiscal_entity: 'Choose fiscal unit and period',
        itgen_bt_powered: 'Powered by Invantive',
        itgen_ct_country: 'Country:',
        itgen_ct_choose_country: 'Choose country',
    });
   
    $translateProvider.translations('fr', {
        itgen_bt_heading: 'VAT Toolkit',
        itgen_bt_toolname_div: 'VAT Entry Lines per Company',
        itgen_bt_toolname_fiscal: 'VAT Entry Lines per Fiscal Unit',
        itgen_bt_button_vat: 'VAT Entry Lines',
        itgen_bt_other: 'More products by Invantive',
        itgen_bt_fiscal_year: 'Fiscal Year',
        itgen_bt_period_from : 'Period from:',
        itgen_bt_up_to : 'up to and including:',
        itgen_bt_division: 'Company:',
        itgen_bt_fiscal_entity: 'Fiscal Unit:',
        itgen_bt_choose_division: 'Choose company and period',
        itgen_bt_choose_fiscal_entity: 'Choose fiscal unit and period',
        itgen_bt_powered: 'Powered by Invantive',
        itgen_ct_country: 'Country:',
        itgen_ct_choose_country: 'Choose country',
    });
   
    $translateProvider.translations('es', {
        itgen_bt_heading: 'VAT Toolkit',
        itgen_bt_toolname_div: 'VAT Entry Lines per Company',
        itgen_bt_toolname_fiscal: 'VAT Entry Lines per Fiscal Unit',
        itgen_bt_button_vat: 'VAT Entry Lines',
        itgen_bt_other: 'More products by Invantive',
        itgen_bt_fiscal_year: 'Fiscal Year',
        itgen_bt_period_from : 'Period from:',
        itgen_bt_up_to : 'up to and including:',
        itgen_bt_division: 'Company:',
        itgen_bt_fiscal_entity: 'Fiscal Unit:',
        itgen_bt_choose_division: 'Choose company and period',
        itgen_bt_choose_fiscal_entity: 'Choose fiscal unit and period',
        itgen_bt_powered: 'Powered by Invantive',
        itgen_ct_country: 'Country:',
        itgen_ct_choose_country: 'Choose country',
    });
   
    $translateProvider.translations('nl', {
        itgen_bt_heading: 'BTW Tools',
        itgen_bt_toolname_div: 'BTW Boekstukregels per Administratie',
        itgen_bt_toolname_fiscal: 'BTW Boekstukregels per Fiscale Eenheid',
        itgen_bt_button_vat: 'BTW Boekstukregels',
        itgen_bt_other: 'Andere Invantive producten',
        itgen_bt_fiscal_year: 'Fiscaal Jaar',
        itgen_bt_period_from : 'Periode van:',
        itgen_bt_up_to : 'tot en met:',
        itgen_bt_division: 'Bedrijf:',
        itgen_bt_fiscal_entity: 'Fiscale eenheid:',
        itgen_bt_choose_division: 'Kies administratie en periode',
        itgen_bt_choose_fiscal_entity: 'Kies fiscale eenheid en periode',
        itgen_bt_powered: 'Aangedreven door Invantive',
        itgen_ct_country: 'Land:',
        itgen_ct_choose_country: 'Kies land',
    });
}]);

app.config(['$locationProvider', function($locationProvider)
{ 
    $locationProvider.html5Mode({ enabled: true, requireBase: false }); 
}]);
    
//
// When an error is thrown, show error modal.
//
app.factory('errorInterceptor', function ($q)
{
    var preventFurtherRequests = false;

    return {
        request: function (config) {
            if (preventFurtherRequests) {
                return;
            }
            return config || $q.when(config);
        },
        requestError: function(request){
            return $q.reject(request);
        },
        response: function (response) {
            return response || $q.when(response);
        },
        responseError: function (response) {
            if (response && response.status === 500) {
                preventFurtherRequests = true;
                $('.errorMessage.er500, .errorOverlay').fadeIn();
                var errorData = response.data;
                var errorDataStripped = errorData.substring(errorData.lastIndexOf("<body>")+6,errorData.lastIndexOf("</body>"));
                $( errorDataStripped ).appendTo($('.errorMessage')); 
            }
            return $q.reject(response);
        }
    };
});
    
app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('errorInterceptor');    
});
    
//
// Choose tool and proceed to next step.
//        
$( ".bsrOne" ).click(function() {
    $('.slideIn').slideToggle();
    $('.slideIn2').slideToggle(200);
});
$( ".bsrFiscal" ).click(function() {
    $('.slideIn').slideToggle();
    $('.slideIn3').slideToggle(200);
}); 

$( ".bsrBack" ).click(function() {
    $('.slideIn2, .slideIn3').fadeOut(0);
    $('.slideIn').fadeIn();
});